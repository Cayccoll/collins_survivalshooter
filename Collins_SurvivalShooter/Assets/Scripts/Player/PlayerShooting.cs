﻿using UnityEngine;
using UnityEngine.UI;


public class PlayerShooting : MonoBehaviour
{
    public int damagePerShot = 15;
    public float timeBetweenBullets = 0.2f;
    public float range = 10f;


    float timer;
    Ray shootRay = new Ray();
    RaycastHit shootHit;
    int shootableMask;
    ParticleSystem gunParticles;
    LineRenderer gunLine;
    AudioSource gunAudio;
    Light gunLight;
    float effectsDisplayTime = 0.2f;
    public ScoreManager score;
    public static int gunType = 0;
    public Light Flashlight;
    bool flashOnOff = false;
    int numShots = 20;
    float degreeShots = 10f;
    public LineRenderer[] ShotgunLines;
    public Sprite flashlightOff;
    public Sprite flashlightOn;
    public Image FlashlightOffOn;
    public Image GunTypeSprite;
    public Sprite pump;
    public Sprite bolt;
    public Sprite smg;
    public Sprite semi;
    public Sprite shotty;
    public Sprite auto;
    public Sprite aa;
    public Sprite mini;

    


    void Awake ()
    {
        shootableMask = LayerMask.GetMask ("Shootable");
        gunParticles = GetComponent<ParticleSystem> ();
        gunLine = GetComponent <LineRenderer> ();
        gunAudio = GetComponent<AudioSource> ();
        gunLight = GetComponent<Light> ();
    }


    void Update ()
    {
        timer += Time.deltaTime;

        if(Input.GetKeyDown("f") && flashOnOff == false)
        {
            Flashlight.intensity = 1.5f;
            flashOnOff = true;
            FlashlightOffOn.sprite = flashlightOn;

        }

        else if (Input.GetKeyDown("f") && flashOnOff == true)
        {
            Flashlight.intensity = 0f;
            flashOnOff = false;
            FlashlightOffOn.sprite = flashlightOff;
        }

        if (Input.GetButtonDown ("Fire1") && timer >= timeBetweenBullets && Time.timeScale != 0 && gunType == 0) //pistol
        {
            Shoot ();
        }

        if(timer >= timeBetweenBullets * effectsDisplayTime)
        {
            DisableEffects ();
        }

        if (Input.GetButtonDown("Fire1") && timer >= timeBetweenBullets && Time.timeScale != 0 && gunType == 1) //slow pump
        {
            damagePerShot = 25;
            range = 8f;
            timeBetweenBullets = 1f;
            numShots = 4;
            degreeShots = 8f;
            GunTypeSprite.sprite = pump;
            Shotgun();
        }

        else if (Input.GetButtonDown("Fire1") && timer >= timeBetweenBullets && Time.timeScale != 0 && gunType == 2) //rifle

        {
            damagePerShot = 100;
            range = 100f;
            timeBetweenBullets = 1f;
            GunTypeSprite.sprite = bolt;
            Shoot();
        }

        else if (Input.GetButton("Fire1") && timer >= timeBetweenBullets && Time.timeScale != 0 && gunType == 3) //smg

        {
            damagePerShot = 5;
            range = 10f;
            timeBetweenBullets = .05f;
            GunTypeSprite.sprite = smg;
            Shoot();
        }

        else if (Input.GetButtonDown("Fire1") && timer >= timeBetweenBullets && Time.timeScale != 0 && gunType == 4) //Semi Auto

        {
            damagePerShot = 75;
            range = 50f;
            timeBetweenBullets = .3f;
            GunTypeSprite.sprite = semi;
            Shoot();
        }

        else if (Input.GetButtonDown("Fire1") && timer >= timeBetweenBullets && Time.timeScale != 0 && gunType == 5) //Better Pump Shotty

        {
            damagePerShot = 30;
            range = 10f;
            timeBetweenBullets = .4f;
            numShots = 7;
            degreeShots = 10f;
            GunTypeSprite.sprite = shotty;
            Shotgun();
        }

        else if (Input.GetButton("Fire1") && timer >= timeBetweenBullets && Time.timeScale != 0 && gunType == 6) //Auto Rifle

        {
            damagePerShot = 45;
            range = 35f;
            timeBetweenBullets = .1f;
            GunTypeSprite.sprite = auto;
            Shoot();
        }

        else if (Input.GetButton("Fire1") && timer >= timeBetweenBullets && Time.timeScale != 0 && gunType == 7) //AutoShotty

        {
            damagePerShot = 30;
            range = 20f;
            timeBetweenBullets = .15f;
            numShots = 10;
            degreeShots = 10f;
            GunTypeSprite.sprite = aa;
            Shotgun();
        }

        else if (Input.GetButton("Fire1") && timer >= timeBetweenBullets && Time.timeScale != 0 && gunType == 8) //minigun

        {
            damagePerShot = 10;
            range = 50f;
            timeBetweenBullets = .01f;
            numShots = 2;
            degreeShots = 1f;
            GunTypeSprite.sprite = mini;
            Shotgun();
        }


    }


    public void DisableEffects ()
    {
        gunLine.enabled = false;
        gunLight.enabled = false;

        for (int i = 0; i < ShotgunLines.Length; i++)
        ShotgunLines[i].enabled = false;
    }



    void Shoot ()
    {
        timer = 0f;

        gunAudio.Play ();

        gunLight.enabled = true;

        gunParticles.Stop ();
        gunParticles.Play ();

        gunLine.enabled = true;
        gunLine.SetPosition (0, transform.position);

        shootRay.origin = transform.position;
        shootRay.direction = transform.forward;

        if(Physics.Raycast (shootRay, out shootHit, range, shootableMask))
        {
            EnemyHealth enemyHealth = shootHit.collider.GetComponent <EnemyHealth> ();
            if(enemyHealth != null)
            {
                enemyHealth.TakeDamage (damagePerShot, shootHit.point);
            }
            gunLine.SetPosition (1, shootHit.point);
        }
        else
        {
            gunLine.SetPosition (1, shootRay.origin + shootRay.direction * range);
        }
    }

    void Shotgun()
    {
        timer = 0f;

        gunAudio.Play();

        gunLight.enabled = true;

        gunParticles.Stop();
        gunParticles.Play();

        //gunLine.enabled = true;
       // gunLine.SetPosition(0, transform.position);


        //Blake helped me with the following code
        shootRay.origin = transform.position; 
        for (int i = 0; i < numShots; i++)

        {
            ShotgunLines[i].enabled = true;
            ShotgunLines[i].SetPosition(0, transform.position);

            int j = i ;

            Vector3 myDirection = transform.forward + .01f * ((j - numShots / 2f) * degreeShots * transform.right);
            shootRay.direction = myDirection;

            if (Physics.Raycast(shootRay, out shootHit, range, shootableMask))
            {
                EnemyHealth enemyHealth = shootHit.collider.GetComponent<EnemyHealth>();
                if (enemyHealth != null)
                {
                    enemyHealth.TakeDamage(damagePerShot, shootHit.point);
                }
                ShotgunLines[i].SetPosition(1, shootHit.point);
            }
            else
            {
                ShotgunLines[i].SetPosition(1, shootRay.origin + myDirection * range);
            }
        }

        shootRay.direction = transform.forward;
        /*
        if (Physics.Raycast(shootRay, out shootHit, range, shootableMask))
        {
            EnemyHealth enemyHealth = shootHit.collider.GetComponent<EnemyHealth>();
            if (enemyHealth != null)
            {
                enemyHealth.TakeDamage(damagePerShot, shootHit.point);
            }
            gunLine.SetPosition(1, shootHit.point);
        }
        else
        {
            gunLine.SetPosition(1, shootRay.origin + shootRay.direction * range);
        }
        */
    }
}
