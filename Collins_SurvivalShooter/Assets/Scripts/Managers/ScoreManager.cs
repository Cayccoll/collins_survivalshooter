﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class ScoreManager : MonoBehaviour
{
    public static int score;
    public PlayerShooting gunType;


    Text text;


    void Awake ()
    {
        text = GetComponent <Text> ();
        score = 0;
    }


    void Update ()
    {
        text.text = "Score: " + score;

        if (score >= 20)
        {
            PlayerShooting.gunType = 1;
        }

        if (score >= 50)
        {
            PlayerShooting.gunType = 2;
        }

        if (score >= 80)
        {
            PlayerShooting.gunType = 3;
        }

        if (score >= 130)
        {
            PlayerShooting.gunType = 4;
        }
        if (score >= 200)
        {
            PlayerShooting.gunType = 5;
        }
        if (score >= 300)
        {
            PlayerShooting.gunType = 6;
        }
        if (score >= 400)
        {
            PlayerShooting.gunType = 7;
        }
        if (score >= 500)
        {
            PlayerShooting.gunType = 8;
        }
    }
}
