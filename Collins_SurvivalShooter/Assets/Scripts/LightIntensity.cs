﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LightIntensity : MonoBehaviour {

    public ScoreManager score;
    public Light sceneLight;
    float intensityGoal;

	// Use this for initialization
	void Start () {
        sceneLight.intensity = 1f;
        intensityGoal = 1f;
	}
	
	// Update is called once per frame
	void Update () {
		if (ScoreManager.score >= 100 && sceneLight.intensity == 1f)
        {
            intensityGoal = .15f;
        }

        if (ScoreManager.score >= 200 && sceneLight.intensity == .15f)
        {
            intensityGoal = 1f;
        }

        if(ScoreManager.score >= 300 && sceneLight.intensity == 1f)
        {
            intensityGoal = .15f;
        }

        if(ScoreManager.score >= 400 && sceneLight.intensity == .15f)
        {
            intensityGoal = 1f;
        }

        if(ScoreManager.score >= 500 && sceneLight.intensity == 1f)
        {
            intensityGoal = .15f;
        }

        sceneLight.intensity = Mathf.MoveTowards(sceneLight.intensity, intensityGoal, Time.deltaTime * .2f);
    }
}
